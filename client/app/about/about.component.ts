import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TableService } from '../services/table.service';
import { ToastComponent } from '../shared/toast/toast.component';
import { Table } from '../shared/models/table.model';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  @Input() tables: Table[];
  addTableForm: FormGroup;


  table = new Table();
  dataTable: Table[] = [];
  isLoading = true;
  isEditing = false;

  constructor(
    private tableService: TableService,
    private formBuilder: FormBuilder,
    public toast: ToastComponent
    
    ) { }

    ngOnInit(): void {
      
      this.getData();
      this.addTableForm = this.formBuilder.group({
        fullname:  new FormControl('', Validators.required),
        gender:  new FormControl('', Validators.required),
        age:  new FormControl('', Validators.required),
        contact:  new FormControl('', Validators.required),
        date_touch:  new FormControl('', Validators.required),
        date_vaccine:  new FormControl('', Validators.required),
        type_touch:  new FormControl('', Validators.required),
        sick:  new FormControl('', Validators.required),
        protection:  new FormControl('', Validators.required),
      });
    }

    addDataTable(): void {
      console.log(this.addTableForm.value,'tableform');
      
      this.tableService.addTable(this.addTableForm.value).subscribe(
        res => {
          console.log(res,'res');
          this.dataTable.push(res);
          this.addTableForm.reset();
          this.toast.setMessage('item added successfully.', 'success');
          this.getData();
        },
        error => {
          console.log(error)
          this.toast.setMessage('item added unsuccess.', 'failed');
        }
        
      );
    }


  // getData(): void {
  //   this.tableService.getTables().subscribe(
  //     data => this.dataTable = data,
      
  //     error => console.log(error),
  //     () => this.isLoading = false
  //   );
  //   console.log(this.dataTable,'data');
    
  // }

  getData() {
    this.tableService.getTables().subscribe((result) => {
      this.dataTable =  result;
      this.isLoading = false
      console.log(this.dataTable,'data');
    });
    
  }

 
  Delete(id){
      if (window.confirm('Are you sure you want to permanently delete this item?')) {
        this.tableService.deleteTable(id).subscribe(
          res => {
            // console.log(res,'res');
            // this.dataTable = this.dataTable.filter((elem:any) => elem._id !== data._id);
            this.toast.setMessage('item deleted successfully.', 'success');
            this.getData();
          },
          error => console.log(error)
        );
      }
  }

}


