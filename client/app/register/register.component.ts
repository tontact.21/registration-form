import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { UserService } from '../services/user.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  othertext = false;
  other_medicine = false;
  other_medicine_1 = false;
  startdate = false;
  startdate_other = false;
  other_status = false;
  gotohospital = false;

  symptomArray: any[] = [
    { check: false, value: 'ไข้' },
    { check: false, value: 'ใส่เครื่องช่วยหายใจ' },
    { check: false, value: 'ไอ' },
    { check: false, value: 'เจ็บคอ' },
    { check: false, value: 'ปวดกลา้มเนื้อ' },
    { check: false, value: 'มีน้ํามูก' },
    { check: false, value: 'มีเสมหะ' },
    { check: false, value: 'หายใจลําบาก' },
    { check: false, value: 'ปวดศีรษะ' },
    { check: false, value: 'ถ่ายเหลว' },
    { check: false, value: 'จมูกไม่ได้กลิ่น' },
    { check: false, value: 'ล้ินไม่รับรส' },
    { check: false, value: 'ตาแดง' },
    { check: false, value: 'ผื่น' },
    { check: false, value: 'อื่นๆ' },
  ];
  antibodyIgMCheck = false;
  antibodyIgGCheck = false;
  antibodyNegCheck = false;
  antibodyIgMCheck2 = false;
  antibodyIgGCheck2 = false;
  antibodyNegCheck2 = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      date_of_illness: new FormControl('', [Validators.required]), // Date of illness
      date_of_first_admission: new FormControl('', [Validators.required]), // Date of first admission
      first_hospital: new FormControl('', [Validators.required]),
      f_province_hospital: new FormControl('', [Validators.required]), //Province of the hospital where the first hospital was admitted
      currently_hospital: new FormControl('', [Validators.required]),
      c_province_hospital: new FormControl('', [Validators.required]), //Province of the hospital currently being treated.
      symptoms: new FormControl('', [Validators.required]),
      first_temp: new FormControl('', [Validators.required]),
      xray: new FormControl('', [Validators.required]),
      xray_date: new FormControl('', [Validators.required]),
      xray_result: new FormControl('', [Validators.required]),
      //// CBC TEST ////
      cbc_date: new FormControl(''),
      hb_result: new FormControl(''),
      hct_result: new FormControl(''),
      platelet_count: new FormControl(''),
      /////////////////////

      //// WBC TEST ////
      wbc_N: new FormControl(''),
      wbc_L: new FormControl(''),
      wbc_AL: new FormControl(''), //  Atyp lymph of WBC TEST
      wbc_Mono: new FormControl(''),
      wbc_other: new FormControl(''),

      /////////////////////
      //// RT_PCR TEST ////
      rt_pcr_collected_date: new FormControl('', [Validators.required]),
      rt_pcr_sample: new FormControl('', [Validators.required]),
      rt_pcr_place: new FormControl('', [Validators.required]),
      rt_pcr_result: new FormControl('', [Validators.required]),
      /////////////////////

      //// Antigen TEST ////
      antigen_collected_date: new FormControl('', [Validators.required]),
      antigen_sample: new FormControl('', [Validators.required]),
      antigen_place: new FormControl('', [Validators.required]),
      antigen_result: new FormControl('', [Validators.required]),
      /////////////////////

      //// First Antibody TEST ////
      f_antibody_collected_date: new FormControl('', [Validators.required]),
      f_antibody_sample: new FormControl('', [Validators.required]),
      f_antibody_place: new FormControl('', [Validators.required]),
      f_antibody_IgM: new FormControl(''),
      f_antibody_IgG: new FormControl(''),
      f_antibody_Neg: new FormControl(''),
      /////////////////////

      //// Second Antibody TEST ////
      s_antibody_collected_date: new FormControl('', [Validators.required]),
      s_antibody_sample: new FormControl('', [Validators.required]),
      s_antibody_place: new FormControl('', [Validators.required]),
      s_antibody_IgM: new FormControl(''),
      s_antibody_IgG: new FormControl(''),
      s_antibody_Neg: new FormControl(''),
      /////////////////////

      type_of_patient: new FormControl('', [Validators.required]),
      treatment_type: new FormControl('', [Validators.required]),
      current_place_treatment: new FormControl('', [Validators.required]),
      current_province_treatment: new FormControl('', [Validators.required]),
      medicine_type: new FormControl('', [Validators.required]),
      medicine_startdate: new FormControl('', [Validators.required]),

      othermedicine_type: new FormControl('', [Validators.required]),
      othermedicine_startdate: new FormControl('', [Validators.required]),

      status_patient: new FormControl('', [Validators.required]),
      admit_patient: new FormControl('', [Validators.required]),
    });
  }
  get f() {
    return this.registerForm.controls;
  }
  // setClassUsername(): object {
  //   return { 'has-danger': !this.username.pristine && !this.username.valid };
  // }

  // setClassEmail(): object {
  //   return { 'has-danger': !this.email.pristine && !this.email.valid };
  // }

  // setClassPassword(): object {
  //   return { 'has-danger': !this.password.pristine && !this.password.valid };
  // }
  otherSymptoms(item) {
    console.log(item, 'check');

    if (item == 'อื่นๆ') {
      this.othertext = true;
    } else {
      // alert('inin')
      this.othertext = false;
    }
  }

  antibodyIgMChecked() {
    if (this.antibodyIgMCheck == false) {
      this.antibodyIgMCheck = true
      this.registerForm.get('f_antibody_IgM').setValidators([Validators.required]);
      this.registerForm.get('f_antibody_IgM').updateValueAndValidity();
    } else {
      this.antibodyIgMCheck = false
      this.registerForm.get('f_antibody_IgM').clearValidators();
      this.registerForm.get('f_antibody_IgM').updateValueAndValidity();
      this.registerForm.patchValue({
        f_antibody_IgM: "",
      })
    }
    console.log(this.antibodyIgMCheck, 'check');
  }
  antibodyIgGChecked() {
    if (this.antibodyIgGCheck == false) {
      this.antibodyIgGCheck = true
      this.registerForm.get('f_antibody_IgG').setValidators([Validators.required]);
      this.registerForm.get('f_antibody_IgG').updateValueAndValidity();

    } else {
      this.antibodyIgGCheck = false
      this.registerForm.get('f_antibody_IgG').clearValidators();
      this.registerForm.get('f_antibody_IgG').updateValueAndValidity();
      this.registerForm.patchValue({
        f_antibody_IgG: "",
      })
    }
    console.log(this.antibodyIgMCheck, 'check');
  }
  antibodyNegChecked() {
    if (this.antibodyNegCheck == false) {
      this.antibodyNegCheck = true
      this.registerForm.patchValue({
        f_antibody_Neg: "Neg",
      })
    } else {
      this.antibodyNegCheck = false
      this.registerForm.patchValue({
        f_antibody_Neg: "",
      })
    }
    console.log(this.antibodyNegCheck, 'check');
  }

  antibodyIgMChecked2() {
    if (this.antibodyIgMCheck2 == false) {
      this.antibodyIgMCheck2 = true
      this.registerForm.get('s_antibody_IgM').setValidators([Validators.required]);
      this.registerForm.get('s_antibody_IgM').updateValueAndValidity();
    } else {
      this.antibodyIgMCheck2= false
      this.registerForm.get('s_antibody_IgM').clearValidators();
      this.registerForm.get('s_antibody_IgM').updateValueAndValidity();
      this.registerForm.patchValue({
        s_antibody_IgM: "",
      })
    }
    console.log(this.antibodyIgMCheck2, 'check');
  }
  antibodyIgGChecked2() {
    if (this.antibodyIgGCheck2 == false) {
      this.antibodyIgGCheck2 = true
      this.registerForm.get('s_antibody_IgG').setValidators([Validators.required]);
      this.registerForm.get('s_antibody_IgG').updateValueAndValidity();

    } else {
      this.antibodyIgGCheck2 = false
      this.registerForm.get('s_antibody_IgG').clearValidators();
      this.registerForm.get('s_antibody_IgG').updateValueAndValidity();
      this.registerForm.patchValue({
        s_antibody_IgG: "",
      })
    }
    console.log(this.antibodyIgMCheck2, 'check');
  }
  antibodyNegChecked2() {
    if (this.antibodyNegCheck2 == false) {
      this.antibodyNegCheck2 = true
      this.registerForm.patchValue({
        s_antibody_Neg: "Neg",
      })
    } else {
      this.antibodyNegCheck2 = false
      this.registerForm.patchValue({
        s_antibody_Neg: "",
      })
    }
    console.log(this.antibodyNegCheck2, 'check');
  }

  ///  ยาต้านไวรัส ///
  onchangeOther() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.medicine_type == 'ยาต้านไวรัสอื่นๆ') {
      // alert('in')
      this.other_medicine = true;
      if (this.registerForm.value.medicine_type != 'ไม่ให้') {
        this.startdate = true;
      }
    } else {
      // alert('inin')
      this.other_medicine = false;
      if (this.registerForm.value.medicine_type != 'ไม่ให้') {
        this.startdate = true;
      }
    }
  }

  ///  ยารักษาโควิด 19 อ่ืนๆ ///
  onchangeotherMedicine() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.othermedicine_type == 'ยาอื่นๆ') {
      // alert('in')
      this.other_medicine_1 = true;
      if (this.registerForm.value.othermedicine_type != 'ไม่ให้') {
        this.startdate_other = true;
      }
    } else {
      // alert('inin')
      this.other_medicine_1 = false;
      if (this.registerForm.value.othermedicine_type != 'ไม่ให้') {
        this.startdate_other = true;
      }
    }
  }
  onchangeStatus() {
    console.log(this.registerForm.value.status_patient, 'test');

    if (this.registerForm.value.status_patient == 'อื่นๆ') {
      this.other_status = true;
      this.gotohospital = false;
    } else if (this.registerForm.value.status_patient == 'ส่งตัวไป รพ.') {
      this.gotohospital = true;
      this.other_status = false;
    } else {
      this.other_status = false;
      this.gotohospital = false;
    }
  }

  registerInformation(): void {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.userService.register(this.registerForm.value).subscribe(
        (res) => {
          this.toast.setMessage('you successfully registered!', 'success');
          this.router.navigate(['/login']);
        },
        (error) => this.toast.setMessage('email already exists', 'danger')
      );
    }
  }
}
