import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Table } from '../shared/models/table.model';

@Injectable()
export class TableService {

  constructor(private http: HttpClient) { }

  getTables(): Observable<Table[]> {
    return this.http.get<Table[]>('/api/tables');
  }

  countTables(): Observable<number> {
    return this.http.get<number>('/api/tables/count');
  }

  addTable(table: Table): Observable<Table> {
    console.log(table,'service');
    
    return this.http.post<Table>('/api/table', table);
  }

//   getTable(table: Table): Observable<Table> {
//     return this.http.get<Table>(`/api/cat/${table._id}`);
//   }

//   editTable(table: Table): Observable<any> {
//     return this.http.put(`/api/cat/${table._id}`, table, { responseType: 'text' });
//   }

deleteTable(id) {
    return this.http.delete('api/table/' + id);
  }

}
