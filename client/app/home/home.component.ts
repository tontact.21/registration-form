import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { UserService } from '../services/user.service';
import { ToastComponent } from '../shared/toast/toast.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  othertext = false;
  otheraddress = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public toast: ToastComponent,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      Idcard: new FormControl('', [
        Validators.required,
        // Validators.minLength(2),
        // Validators.maxLength(13),
      ]),
      fullname : new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
      nationality: new FormControl('', [Validators.required]),
      pregnant:new FormControl('', [Validators.required]),
      pregnant_No: new FormControl(''),
      pregnant_Week: new FormControl(''),
      type_of_patient: new FormControl('', [Validators.required]),
      occupation: new FormControl('', [Validators.required]),
      workplace: new FormControl('', [Validators.required]),
      contact_No: new FormControl('', [Validators.required]),
      phone_app: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      houseNo:  new FormControl('', [Validators.required]),
      villageNo:  new FormControl('', [Validators.required]),
      village: new FormControl('', [Validators.required]),
      alley: new FormControl('', [Validators.required]),
      road: new FormControl('', [Validators.required]),
      province: new FormControl('', [Validators.required]),
      sub_district:  new FormControl('', [Validators.required]),
      district: new FormControl('', [Validators.required]),
      zipcode:  new FormControl('', [Validators.required]),
      congenital_disease:  new FormControl('', [Validators.required]),
      smoking:  new FormControl('', [Validators.required]),
      // email: new FormControl('', [
      //   Validators.email,
      //   Validators.required,
      //   Validators.minLength(3),
      //   Validators.maxLength(100),
      // ]),
      // password: new FormControl('', [
      //   Validators.required,
      //   Validators.minLength(6),
      // ]),
      // role: new FormControl('', [Validators.required]),
    });
      // for (let item of this.language) {
      //   if (item.check) {
      //     if(item.value != "อื่นๆ")
      //     this.registerservice.multilanguage(item, response.data).subscribe(response => {
      //       console.log(response)
      //     })
      //   }

      // }
  }
  get f() {
    return this.registerForm.controls;
  }

  
  otherType_of_Patient() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.type_of_patient == "อื่นๆ") {
      // alert('in')
      this.othertext = true;
     
    } else {
      // alert('inin')
      this.othertext = false;
    }

  }
  otherAddress() {
    // alert(this.form.value.religion)
    if (this.registerForm.value.address == "อื่นๆ") {
      // alert('in')
      this.otheraddress = true;
     
    } else {
      // alert('inin')
      this.otheraddress = false;
    }

  }
  registerInformation(): void {
    console.log(  this.registerForm.value.pregnant,"test");
   
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.userService.register(this.registerForm.value).subscribe(
        (res) => {
          this.toast.setMessage('you successfully registered!', 'success');
          this.router.navigate(['/login']);
        },
        (error) => this.toast.setMessage('email already exists', 'danger')
      );
    }
  }
}
