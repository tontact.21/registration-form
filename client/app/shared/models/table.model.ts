export class Table {
    fullname?: String;
    gender?: String;
    age?: Number;
    contact?:String;
    date_touch?: Date;
    date_vaccine?:Date;
    type_touch?: String;
    sick?: String;
    protection?: String;
  }
  