import * as mongoose from 'mongoose';
var Schema = mongoose.Schema;
const tableSchema = new mongoose.Schema({
  fullname: String,
    gender: String,
    age: Number,
    contact:String,
    date_touch: Date,
    date_vaccine :Date,
    type_touch: String,
    sick: String,
    protection: String,
  created_at: { type: Date, default: Date.now() },
  updated_at: { type: Date, default: Date.now() },
});

const Table = mongoose.model('Table', tableSchema);

export interface TableModel {
    fullname: String;
    gender: String;
    age: String;
    contact:String;
    date_touch: Date;
    date_vaccine :Date;
    type_touch: String;
    sick: String;
    protection: String;
}

export default Table;
